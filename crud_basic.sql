-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2020 at 01:36 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud_basic`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `address`, `created_at`, `updated_at`) VALUES
(1, 'haviet', 'ha@gmail.com', '123456', '0399776789', 'Bac Giang', NULL, NULL),
(2, 'tienanh', 'anhht@gmail.com', '123456', '0456789345', 'Tan My, Bac Giang', NULL, NULL),
(3, 'thao', 'thao@gmail.com', '123456', '0964567645', 'Bac Giang', '2020-04-15 09:43:27', '2020-04-15 09:43:27'),
(4, 'kienvutrung', 'kien@gmail.com', '123456', '0976876567', 'Bac Giang', '2020-04-15 09:46:32', '2020-04-15 09:46:32'),
(5, 'huygay', 'huygay@gmail.com', '123456', '01697654556', 'Hai phong', '2020-04-15 09:59:37', '2020-04-15 09:59:37'),
(27, 'tien anh', '0399881175', '123456', '0399888567', 'ha noi, ho tay', '2020-04-15 10:06:56', '2020-04-15 10:06:56'),
(51, 'php', 'nam@gmail.com', '123456', '0399888567', 'Bac Giang', '2020-04-15 10:26:48', '2020-04-15 10:26:48'),
(53, 'php 4-1', 'kien123@gmail.com', '123456', '4561234567', 'Bac Giang', '2020-04-15 10:28:02', '2020-04-15 10:28:02'),
(54, 'PHP 22', 'hadz@gmail.com', '123456', '0987567898', 'ha noi', '2020-04-15 10:31:18', '2020-04-15 10:31:18'),
(55, 'PHP 1', 'tienanh1111111@gmail.com', '111', '1234567890', 'ha noi', '2020-04-15 10:33:44', '2020-04-15 10:33:44'),
(60, 'php 4-1', 'ha234@gmail.com', '111', '1234567890', 'hanoi', '2020-04-15 10:37:08', '2020-04-15 10:37:08'),
(65, 'php 1-2 jvb', 'hadzzzzzzzzzz@gmail.com', '1234567', '1234567890', 'ha dong, ha noi', '2020-04-15 10:45:03', '2020-04-16 01:23:44'),
(66, 'nguyenvietha', 'viethanguyen15@gmai.com', '123456', '0399887789', 'Bac Giang', '2020-04-15 10:46:31', '2020-04-15 10:46:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
