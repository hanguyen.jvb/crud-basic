<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function list()
    {
        $data['users'] = User::orderBy('id', 'desc')->get();
        return view('user.list', $data);
    }

    public function store(Request $request)
    {
        $user = User::create($request->all());
        return response()->json(['result' => 'true', 'users' => $user], 200);
    }

    public function show($idUser)
    {
        $data['user'] = User::find($idUser);
        return response()->json($data, 200);
    }

    public function edit($idUser)
    {
        $data['user'] = User::find($idUser);
        return response()->json($data, 200);
    }

    public function update(Request $request, $idUser)
    {
        $data = $request->all();
        $user = User::find($idUser);

        if ($user->update($data)) {
            return response()->json(['user' => $user], 200);
        }
    }

    public function destroy($idUser)
    {
        $data['removeUser'] = User::find($idUser)->delete();
        return response()->json($data, 200);
    }
}
