$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function() {
    $('.add').click(function() {
        $("#add").modal('show');
        $(".addUser").click(function() {
            let name = $("#name").val();
            let email = $("#email").val();
            let password = $("#password").val();
            let phone = $("#phone").val();
            let address = $("#address").val();

            $.ajax({
                url : '/users/add',
                type : 'post',
                data : {
                    name : name,
                    email: email,
                    password : password,
                    phone: phone,
                    address: address,
                },
                dataType : 'json',
                success : function(data) {
                    if (data.result == 'true') {
                        toastr.success('add user successfully')
                        $('#add').modal('hide');
                        setTimeout(function() {
                            location.reload();
                        }, 3000);
                    }
                }
            });
        });
    });

    $('.show').click(function() {
        $("#show").modal('show');
        var id = $(this).data('id');

        $.ajax({
            url : '/users/show/' + id,
            dataType : 'json',
            type : 'get',
            success : function($data) {
                console.log($data);
                $('#detail-name').text($data.user.name);
                $('#detail-email').text($data.user.email);
                $('#detail-phone').text($data.user.phone);
                $('#detail-address').text($data.user.address);
            }
        });
    });

    $('.edit').click(function() {
        $('#edit').modal('show');
        var id = $(this).data('id');

        $.ajax({
            url : '/users/edit/' + id,
            dataType : 'json',
            type : 'get',
            success : function($data) {
                console.log($data);
                $('#edit-name').val($data.user.name);
                $('#edit-email').val($data.user.email);
                $('#edit-phone').val($data.user.phone);
                $('#edit-address').val($data.user.address);
            }
        });

        $('.editUser').click(function() {
            let name = $('#edit-name').val();
            let email = $('#edit-email').val();
            let password = $('#edit-password').val();
            let phone = $('#edit-phone').val();
            let address = $('#edit-address').val();

            $.ajax({
                url : '/users/update/' + id,
                dataType : 'json',
                type : 'put',
                data : {
                    name : name,
                    email : email,
                    password : password,
                    phone : phone,
                    address : address,
                },
                success : function(data) {
                    $('#edit').modal('hide');
                    toastr.success('edit user successfully');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                }
            });
        });
    });

    $('.delete').click(function() {
        $('#delete').modal('show');
        let id = $(this).data('id');

        $('.deleteUser').click(function() {
            $.ajax({
                url : '/users/' + id,
                dataType : 'json',
                type : 'delete',
                success : function(data) {
                    $('#delete').modal('hide');
                    toastr.success('delete user successfully');
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                }
            });
        })
    });
});
