<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('users', 'UserController@list')->name('users.list');
Route::post('users/add', 'UserController@store')->name('users.store');
Route::get('users/show/{idUser}', 'UserController@show')->name('users.show');
Route::get('users/edit/{idUser}', 'UserController@edit')->name('users.edit');
Route::put('users/update/{idUser}', 'UserController@update')->name('users.update');
Route::delete('users/{idUser}', 'UserController@destroy')->name('users.destroy');
