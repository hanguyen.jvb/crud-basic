@push('css')
    <link rel="stylesheet" href="{{ asset('css/list-user.css') }}"/>
@endpush
@extends('master.master')
@section('title', 'list user')
@section('content')
<button type="button" class="btn btn-primary add" style="margin-left: 270px; margin-top: 20px;">Add +</button>
<table class="table table-user">
    <thead class="thead-dark">
        <tr>
            <th scope="col">id</th>
            <th scope="col">name</th>
            <th scope="col">email</th>
            <th scope="col">phone</th>
            <th scope="col">address</th>
            <th scope="col">show</th>
            <th scope="col">edit</th>
            <th scope="col">delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
            <tr>
                <th scope="row">{{ $user->id }}</th>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone }}</td>
                <td>{{ $user->address }}</td>
                <td><button type="button" class="btn btn-info show" data-id="{{ $user->id }}">Show</button></td>
                <td><button type="button" class="btn btn-warning edit" data-id="{{ $user->id }}">Edit</button></td>
                <td><button type="button" class="btn btn-danger delete" data-id="{{ $user->id }}">Delete</button></td>
            </tr>
        @endforeach
    </tbody>
</table>

<!--add-->
<div class="modal fade" id="add">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">add user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post">
                    <div class="form-group">
                        <label>name</label>
                        <input type="text" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label>email</label>
                        <input type="email" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label>password</label>
                        <input type="password" class="form-control" id="password">
                    </div>
                    <div class="form-group">
                        <label>phone</label>
                        <input type="text" class="form-control" id="phone">
                    </div>
                    <div class="form-group">
                        <label>address</label>
                        <input type="text" class="form-control" id="address">
                    </div>
                    <button type="button" class="btn btn-primary addUser">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </form>
            </div>
        </div>
    </div>
</div>
<!--end add-->

<!--show-->
<div class="modal fade" id="show">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">show user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            Name: <span id="detail-name"></span><br>
            Email: <span id="detail-email"></span><br>
            Phone: <span id="detail-phone"></span><br>
            Address: <span id="detail-address"></span>
            </div>
        </div>
    </div>
</div>
<!--end show-->

<!--edit-->
<div class="modal fade" id="edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">edit user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="">
                    <div class="form-group">
                        <label>name</label>
                        <input type="text" class="form-control" id="edit-name">
                    </div>
                    <div class="form-group">
                        <label>email</label>
                        <input type="email" class="form-control" id="edit-email">
                    </div>
                    <div class="form-group">
                        <label>password</label>
                        <input type="password" class="form-control" id="edit-password">
                    </div>
                    <div class="form-group">
                        <label>phone</label>
                        <input type="text" class="form-control" id="edit-phone">
                    </div>
                    <div class="form-group">
                        <label>address</label>
                        <input type="text" class="form-control" id="edit-address">
                    </div>
                    <button type="button" class="btn btn-success editUser">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </form>
            </div>
        </div>
    </div>
</div>
<!--end edit-->

<!--delete-->
<div class="modal fade" id="delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">delete user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-danger deleteUser">Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--end delete-->
@endsection
